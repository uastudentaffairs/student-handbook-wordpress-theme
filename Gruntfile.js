module.exports = function(grunt) {

    grunt.initConfig({
        sass: {
            studenthandbook: {
                options: {
                    sourcemap: 'none',
                    style: 'compressed',
                    noCache: true,
                    update: false,
                    loadPath: '../themes/student-affairs-framework/scss'
                },
                files: [{
                    expand: true,
                    src: '*.scss',
                    cwd: 'scss',
                    dest: 'css',
                    ext: '.min.css'
                }]
            }
        },
        uglify: {
            options: {
                mangle: false,
                compress: false
            },
            studenthandbook: {
                files: [{
                    expand: true,
                    src: [ '**/*.js' ],
                    cwd: 'js',
                    dest: 'js',
                    ext: '.min.js'
                }]
            }
        },
        watch: {
            studenthandbookjs: {
                files: [ 'js/*' ],
                tasks: [ 'newer:uglify:studenthandbook' ]
            },
            studenthandbooksass: {
                files: [ 'scss/*' ],
                tasks: [ 'sass:studenthandbook' ]
            }
        }
    });

    // Load our dependencies
    grunt.loadNpmTasks( 'grunt-contrib-sass' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-watch' );
    grunt.loadNpmTasks( 'grunt-newer' );

    // Register our tasks
   	grunt.registerTask( 'default', [ 'newer:sass', 'newer:uglify', 'watch' ] );

    // Register a watch function
    grunt.event.on( 'watch', function( action, filepath, target ) {
        grunt.log.writeln( target + ': ' + filepath + ' has ' + action );
    });

};