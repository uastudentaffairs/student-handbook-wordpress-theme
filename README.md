# UA Student Affairs - Student Handbook - WordPress Theme #

This child theme of the [UA Student Affairs WordPress Framework](https://bitbucket.org/uastudentaffairs/student-affairs-framework-wordpress-theme) is for [The University of Alabama Student Handbook](https://studenthandbook.ua.edu/).

While the design is intended solely for the Student Handbook, the code is shared for educational purposes. Feel free to explore, copy, modify, and share the code.

If you have any questions or comments, contact the author: [Rachel Carden](mailto:rmcarden@ua.edu).

### A few facts about this repository: ###

* It was created for [The University of Alabama Student Handbook](https://studenthandbook.ua.edu/).
* It was built for use with the [WordPress](https://wordpress.org/) content management system.
* It is a [WordPress child theme](https://codex.wordpress.org/Child_Themes). The parent: [The UA Student Affairs WordPress Framework](https://bitbucket.org/uastudentaffairs/student-affairs-framework-wordpress-theme).
* The front-end design is built upon the [Foundation](http://foundation.zurb.com/) framework.

### How do I get set up? ###

* Info coming soon.

### Contribution guidelines ###

* Info coming soon.

### Contact Information ###

* [Rachel Carden](mailto:rmcarden@ua.edu)