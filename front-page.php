<?php

get_header();

// @TODO setup links to display in order of most viewed via google analytics
// @TODO Include all children for each category but only display the top 3, the rest will be included when printed

// Set the blog URL
$blog_url = get_bloginfo( 'url' );

// Set the images folder
$images_folder = get_stylesheet_directory_uri() . '/images/';

?><div class="sa-handbook-featured-grid">
	<div class="row">
		<div class="small-12 columns">
			<ul class="block-grid small-block-grid-1 medium-block-grid-2 large-block-grid-3">
				<li>
					<div class="item-header">
						<a href="<?php echo $blog_url; ?>/academic-policies/">
							<img class="item-image" src="<?php echo $images_folder; ?>puzzle-bgs/puzzle-bg-1.png" alt="" />
							<h2 class="item-title">Academic Policies</h2>
						</a>
					</div>
					<div class="item-text">
						<ul>
							<li><a href="<?php echo $blog_url; ?>/academic-policies/general/">General Academic Policies</a></li>
							<li><a href="<?php echo $blog_url; ?>/academic-policies/academic-integrity-policy/">Academic Integrity Policy</a></li>
							<li><a href="<?php echo $blog_url; ?>/academic-policies/academic-grievance-policy/">Academic Grievance Policy</a></li>
						</ul>
						<p class="view"><a class="button secondary expand" href="<?php echo $blog_url; ?>/academic-policies/">View all policies</a></p>
					</div>
				</li>
				<li>
					<div class="item-header">
						<a href="<?php echo $blog_url; ?>/policies-relevant-to-students/">
							<img class="item-image" src="<?php echo $images_folder; ?>puzzle-bgs/puzzle-bg-2.png" alt="" />
							<h2 class="item-title">Policies Relevant to Students</h2>
						</a>
					</div>
					<div class="item-text">
						<ul>
							<li><a href="<?php echo $blog_url; ?>/policies-relevant-to-students/code-of-student-conduct/">Code of Student Conduct</a></li>
							<li><a href="<?php echo $blog_url; ?>/policies-relevant-to-students/hazing/">Hazing</a></li>
							<li><a href="<?php echo $blog_url; ?>/policies-relevant-to-students/smoke-free-campus-environment/">Smoke-Free Campus Environment</a></li>
						</ul>
						<p class="view"><a class="button secondary expand" href="<?php echo $blog_url; ?>/policies-relevant-to-students/">View all policies</a></p>
					</div>
				</li>
				<li>
					<div class="item-header">
						<a href="<?php echo $blog_url; ?>/student-appellant-and-grievance-avenues/">
							<img class="item-image" src="<?php echo $images_folder; ?>puzzle-bgs/puzzle-bg-3.png" alt="" />
							<h2 class="item-title lots-o-text">Student Appellant and Grievance Avenues</h2>
						</a>
					</div>
					<div class="item-text">
						<ul>
							<li><a href="<?php echo $blog_url; ?>/student-appellant-and-grievance-avenues/academic-grievances/">Academic Grievances</a></li>
							<li><a href="<?php echo $blog_url; ?>/student-appellant-and-grievance-avenues/student-records/">Student Records</a></li>
							<li><a href="<?php echo $blog_url; ?>/student-appellant-and-grievance-avenues/financial-aid/">Financial Aid</a></li>
						</ul>
						<p class="view"><a class="button secondary expand" href="<?php echo $blog_url; ?>/student-appellant-and-grievance-avenues/">View all avenues</a></p>
					</div>
				</li>
				<li>
					<div class="item-header">
						<a href="<?php echo $blog_url; ?>/student-organization-policies/">
							<img class="item-image" src="<?php echo $images_folder; ?>puzzle-bgs/puzzle-bg-4.png" alt="" />
							<h2 class="item-title">Student Organization Policies</h2>
						</a>
					</div>
					<div class="item-text">
						<ul>
							<li><a href="<?php echo $blog_url; ?>/student-organization-policies/non-fraternal-student-organizations/">Non-Fraternal Student Organizations</a></li>
							<li><a href="<?php echo $blog_url; ?>/student-organization-policies/social-guidelines/">Social Guidelines</a></li>
							<li><a href="<?php echo $blog_url; ?>/student-organization-policies/grounds-use/">Grounds Use</a></li>
						</ul>
						<p class="view"><a class="button secondary expand" href="<?php echo $blog_url; ?>/student-organization-policies/">View all policies</a></p>
					</div>
				</li>
				<li>
					<div class="item-header">
						<a href="<?php echo $blog_url; ?>/sga/">
							<img class="item-image" src="<?php echo $images_folder; ?>puzzle-bgs/puzzle-bg-5.png" alt="" />
							<h2 class="item-title">Student Government Association</h2>
						</a>
					</div>
					<div class="item-text">
						<ul>
							<li><a href="<?php echo $blog_url; ?>/sga/constitution/">SGA Constitution</a></li>
							<li><a href="<?php echo $blog_url; ?>/sga/code-of-laws/">SGA Code of Laws</a></li>
							<li><a href="<?php echo $blog_url; ?>/sga/election-policy-and-procedures/">Election Policy and Procedures</a></li>
						</ul>
						<p class="view"><a class="button secondary expand" href="<?php echo $blog_url; ?>/sga/">View all documents</a></p>
					</div>
				</li>
				<li>
					<div class="item-header">
						<a href="<?php echo $blog_url; ?>/student-resources/">
							<img class="item-image" src="<?php echo $images_folder; ?>puzzle-bgs/puzzle-bg-6.png" alt="" />
							<h2 class="item-title">Student Resources</h2>
						</a>
					</div>
					<div class="item-text">
						<ul>
							<li><a href="<?php echo $blog_url; ?>/student-resources/student-media/">Student Media</a></li>
							<li><a href="<?php echo $blog_url; ?>/student-resources/student-health-center/">Student Health Center</a></li>
							<li><a href="<?php echo $blog_url; ?>/student-resources/student-identification/">Student Identification</a></li>
						</ul>
						<p class="view"><a class="button secondary expand" href="<?php echo $blog_url; ?>/student-resources/">View all resources</a></p>
					</div>
				</li>
				<li>
					<div class="item-header">
						<a href="<?php echo $blog_url; ?>/reporting-improper-conduct/">
							<img class="item-image" src="<?php echo $images_folder; ?>puzzle-bgs/puzzle-bg-7.png" alt="" />
							<h2 class="item-title">Reporting Improper Conduct</h2>
						</a>
					</div>
				</li>
				<li>
					<div class="item-header">
						<a href="<?php echo $blog_url; ?>/emergency-resources/">
							<img class="item-image" src="<?php echo $images_folder; ?>puzzle-bgs/puzzle-bg-8.png" alt="" />
							<h2 class="item-title">Emergency Resources</h2>
						</a>
					</div>
				</li>
				<li>
					<div class="item-header">
						<a href="<?php echo $blog_url; ?>/other-helpful-websites/">
							<img class="item-image" src="<?php echo $images_folder; ?>puzzle-bgs/puzzle-bg-9.png" alt="" />
							<h2 class="item-title">Other Helpful Websites</h2>
						</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div><?php

get_footer();