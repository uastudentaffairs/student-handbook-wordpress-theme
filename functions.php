<?php

// Include filters
require_once( STYLESHEETPATH . '/includes/filters.php' );

// Include shortcodes
require_once( STYLESHEETPATH . '/includes/shortcodes.php' );

// Setup styles and scripts
add_action( 'wp_enqueue_scripts', function () {

    // Get the theme directory
    $sa_theme_dir = trailingslashit( get_stylesheet_directory_uri() );

    // Enqueue the theme stylesheet
    wp_enqueue_style( 'sa-handbook', $sa_theme_dir . 'css/sa-handbook.min.css' );

    // Enqueue the theme print styles
    wp_enqueue_style( 'sa-handbook-print', $sa_theme_dir . 'css/sa-handbook-print.min.css', array( 'sa-handbook' ), false, 'print' );

    // Enqueue the theme script
    wp_enqueue_script( 'sa-handbook', $sa_theme_dir . 'js/sa-handbook.min.js', array( 'jquery' ), false, true );

}, 20 );