<?php

// Add the print button before the page title
add_action( 'sa_framework_before_page_title', function() {
	//global $post;

	// Get the setting
	//if ( $display_print_button = ( $display_print_button_field = get_post_meta( $post->ID, 'sa_handbook_display_print_button', true ) ) && strcasecmp( 'yes', $display_print_button_field ) == 0 ? true : false ) {

	// Add the print button to every page except the home page
	if ( ! is_front_page() ) {
		?><span class="hide-on-print print-page-button" onclick="window.print()">Print this page</span><?php
	}

	//}

});

// Print the handbook header
add_action( 'sa_framework_print_header', 'sa_handbook_print_header' );
function sa_handbook_print_header() {
	global $sa_framework_site_slug;

	// Setup header wrapper classes - has UA square by default
	$header_wrapper_classes = array( "sa-{$sa_framework_site_slug}" );

	?><div id="sa-header-wrapper" class="<?php echo implode( ' ', $header_wrapper_classes ); ?>">
		<div class="row">
			<div class="small-12 columns">

				<div id="sa-header"><?php

					do_action( 'sa_framework_before_site_title' );

					?><div class="sa-site-title-wrapper">
						<a href="<?php echo get_bloginfo( 'url' ); ?>">

							<img class="ua-square" src="<?php echo get_template_directory_uri(); ?>/images/logos/ua-square.svg" alt="" />
							<h1 class="sa-site-title">
								<span class="line first-line">The University of Alabama</span>
								<span class="line second-line">Student Handbook</span>
							</h1>

						</a>
					</div><?php

					do_action( 'sa_framework_after_site_title' );

				?></div> <!-- #sa-header -->

			</div>
		</div>
	</div> <!-- #sa-header-wrapper --><?php

}

// Filter the main column CSS selectors
add_filter( 'sa_framework_main_column_css_selectors', function ( $selectors, $column_id ) {

	// Tells us to make the sidebar scroll with the content
	if ( in_array( 'sa-sidebar', $selectors ) && in_array( 'sa-left-sidebar', $selectors ) ) {
		$selectors = array_merge( $selectors, array( 'scroll-with-content', 'align-with-content' ) );
	}

	return $selectors;
}, 10, 2 );

// Tell it to show the submenu header
add_filter( 'sa_framework_display_submenu_header', function( $display_submenu_header, $submenu_header, $menu_args ) {
	return true;
}, 100, 3 );