<?php

// Print grid list of handbook children sections
add_shortcode( 'print_student_handbook_section_list', function ( $args ) {

	// Setup the arguments
	$defaults = array(
		'parent_page'   => 0,
		'orderby'       => 'menu_order',
		'order'         => 'ASC',
	);
	$args = wp_parse_args( $args, $defaults );

	// Build content
	$content = NULL;

	// If we have a parent page...
	if ( $args[ 'parent_page' ] > 0 ) {

		// Get its children
		if ( $children = get_children( array(
			'post_parent'   => $args[ 'parent_page' ],
			'post_type'     => 'page',
			'numberposts'   => -1,
			'post_status'   => 'publish',
			'orderby'       => $args[ 'orderby' ],
			'order'         => $args[ 'order' ],
		) ) ) {

			$content = '<div class="sa-handbook-section-list">';

				foreach( $children as $child ) {

					$content .= '<div class="list-item">';

						// Include header
						$content .= '<h2 class="item-header"><a href="' . get_permalink( $child->ID ) . '">' . apply_filters( 'the_title', $child->post_title, $child->ID ) . '</a></h2>';

						// Include an excerpt
						$child_excerpt = ! empty( $child->post_excerpt ) ? $child->post_excerpt : $child->post_content;
						if ( ! empty( $child_excerpt ) ) {
							$content .= wpautop( wp_trim_words( $child_excerpt, 30 ) );
						}

					$content .= '</div>';

				}

			$content .= '</div>';

		}

	}

	return $content;

});

//  Register a UI for the shortcodes
if ( function_exists( 'shortcode_ui_register_for_shortcode' ) ) {

	shortcode_ui_register_for_shortcode(
		'print_student_handbook_section_list',
		array(
			'label'         => 'Print Student Handbook Children Sections',
			'listItemImage' => 'dashicons-list-view',
			'post_type'     => array( 'page' ),
			'attrs'         => array(
				array(
					'label'     => 'Select Parent Page',
					'attr'      => 'parent_page',
					'type'      => 'post_select',
					'query'     => array( 'post_type' => 'page' ),
					'multiple'  => false,
				),
				array(
					'label'     => 'How do you want to order the children?',
					'attr'      => 'orderby',
					'type'      => 'radio',
					'value'     => 'menu_order',
					'options'   => array(
						'menu_order'    => 'Page Order',
						'post_title'    => 'Page Title',
					),
				),
				array(
					'label'     => 'Order the children ascendingly or descendingly?',
					'attr'      => 'order',
					'type'      => 'radio',
					'value'     => 'ASC',
					'options'   => array(
						'ASC'   => 'Ascending',
						'DESC'  => 'Descending',
					),
				),
			),
		)
	);

}