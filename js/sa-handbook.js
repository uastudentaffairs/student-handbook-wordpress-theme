(function( $ ) {
    'use strict';

    // Adjust the main menu when its ready
    $( '#sa-handbook-main-menu-wrapper' ).ready( function() {
        sa_handbook_adjust_main_menu();
    });

    // When the window is resized...
    $(window).resize(function() {

        // Adjust the main menu when the window is resized
        sa_handbook_adjust_main_menu();

    });

    // Setup categories menu item to "open"
    var $sa_handbook_menu_categories_wrapper = $( '#sa-handbook-main-menu-categories-wrapper' );
    var $sa_handbook_open_categories = $( '.open-sa-handbook-categories' );
    $sa_handbook_open_categories.on( 'click touchstart', function( $event ) {
        $event.preventDefault();

        // If it's already open, close it
        if ( $sa_handbook_menu_categories_wrapper.hasClass( 'open' ) ) {
            $sa_handbook_menu_categories_wrapper.removeClass( 'open' );
            $sa_handbook_open_categories.removeClass( 'open' );
            $('html').removeClass('no-overflow');
        }

        // Otherwise open it
        else {
            $sa_handbook_menu_categories_wrapper.addClass( 'open' );
            $sa_handbook_open_categories.addClass( 'open' );
            $('html').addClass('no-overflow');
        }

    });

    // Setup search field listener to "activate"
    var $sa_handbook_main_menu_search_form = $( '#sa-handbook-main-menu-search-form' );
    $( '#sa-handbook-main-menu-search-field' ).on( 'focus blur input', function( $event ) {

        switch( $event.type ) {

            case 'blur':
                // Only "deactivate" if the field is empty
                if ( $( this ).val() == '' ) {
                    $sa_handbook_main_menu_search_form.removeClass( 'active' );
                }
                break;

            case 'focus':
                // Only "activate" if the field is not empty
                if ( $( this ).val() != '' ) {
                    $sa_handbook_main_menu_search_form.addClass('active');
                }
                break;

            case 'input':
                // Only "activate" if the field is not empty
                if ( $( this ).val() != '' ) {
                    $sa_handbook_main_menu_search_form.addClass('active');
                } else {
                    $sa_handbook_main_menu_search_form.removeClass('active');
                }
                break;

        }

    });

    // Setup analytics to track each time someone prints a page
    $( '.print-page-button' ).on( 'click', function() {
        ga('send', 'event', 'button', 'click', 'print-page-button');
    });

    ////// FUNCTIONS //////

    // Adjust elements in the main menu according to screen size
    function sa_handbook_adjust_main_menu() {

        // Tweak the main menu
        var $sa_handbook_main_menu_wrapper = $( '#sa-handbook-main-menu-wrapper');

        // Get the main menu
        var $sa_handbook_main_menu = $sa_handbook_main_menu_wrapper.children( '.sa-handbook-main-menu' );

        // Get the search form
        var $sa_handbook_search_form = $sa_handbook_main_menu_wrapper.children( '.search-form' );

        if ( $(window).width() < 680 ) {

            // Remove the resized class
            $sa_handbook_main_menu_wrapper.removeClass( 'form-is-resized' );

            // Clear any attributes
            $sa_handbook_search_form.removeAttr( 'style' );

        } else {

            // Add the resized class
            $sa_handbook_main_menu_wrapper.addClass( 'form-is-resized' );

            // Adjust the form width to fill the area
            $sa_handbook_search_form.width( $sa_handbook_main_menu_wrapper.width() - $sa_handbook_main_menu.width() );

        }

    }

})( jQuery );