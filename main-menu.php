<?php

// Get the blog URL
$blog_url = get_bloginfo( 'url' );

?><div id="sa-main-menu-wrapper" class="enable-sticky">

	<div class="row">
		<div class="small-12 columns">

			<div id="sa-main-menu"><?php

				?><div id="sa-handbook-main-menu-wrapper">

					<ul class="sa-handbook-main-menu">
						<li class="categories open-sa-handbook-categories"><a href="#"><span>Categories</span></a></li>
						<li class="ua-mission"><a href="https://ua.edu/mission.html" target="_blank"><span class="show-when-open">UA</span> Mission</a></li>
						<li class="contact"><a href="<?php echo $blog_url; ?>/contact/">Contact <span class="show-when-open">Us</span></a></li>
					</ul><?php

					// Get the search query
					$search_query = get_search_query();

					?><form role="search" method="get" id="sa-handbook-main-menu-search-form" class="search-form<?php echo ! empty( $search_query ) ? ' active' : null; ?>" action="<?php echo esc_url( home_url( '/' ) ); ?>">
						<label>
							<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ); ?></span>
							<input type="search" id="sa-handbook-main-menu-search-field" class="search-field" placeholder="Search the handbook" value="<?php echo $search_query; ?>" name="s" title="Search The University of Alabama Student Handbook" />
						</label>
						<input type="submit" class="search-submit" value="Search" />
					</form>

				</div> <!-- #sa-handbook-main-menu-wrapper -->

			</div>

		</div>
	</div>

	<div id="sa-handbook-main-menu-categories-wrapper">

		<span class="open-sa-handbook-categories close-icon"><span class="screen-reader-text">Close</span></span>

		<div class="row">
			<div class="small-12 columns">

				<div id="sa-handbook-main-menu-categories">
					<div class="category-row">
						<div class="category"><a href="<?php echo $blog_url; ?>/academic-policies/"><span>Academic Policies</span></a></div>
						<div class="category"data-equalizer-watch><a href="<?php echo $blog_url; ?>/policies-relevant-to-students/"><span>Policies Relevant to Students</span></a></div>
						<div class="category"data-equalizer-watch><a href="<?php echo $blog_url; ?>/student-appellant-and-grievance-avenues/"><span>Student Appelant and Grievance Avenues</span></a></div>
					</div>
					<div class="category-row">
						<div class="category"><a href="<?php echo $blog_url; ?>/student-organization-policies/"><span>Student Organization Policies</span></a></div>
						<div class="category"><a href="<?php echo $blog_url; ?>/sga/"><span>Student Government Association</span></a></div>
						<div class="category"><a href="<?php echo $blog_url; ?>/student-resources/"><span>Student Resources</span></a></div>
					</div>
					<div class="category-row">
						<div class="category"><a href="<?php echo $blog_url; ?>/reporting-improper-conduct/"><span>Reporting Improper Conduct</span></a></div>
						<div class="category"><a href="<?php echo $blog_url; ?>/emergency-resources/"><span>Emergency Resources</span></a></div>
						<div class="category"><a href="<?php echo $blog_url; ?>/other-helpful-websites/"><span>Other Helpful Websites</span></a></div>
					</div>
				</div> <!-- #sa-handbook-main-menu-categories -->

			</div>
		</div>

	</div> <!-- #sa-handbook-main-menu-categories-wrapper -->

</div> <!-- #sa-main-menu-wrapper -->
